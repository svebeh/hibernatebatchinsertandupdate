package de.berripi.main;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="ARTIKEL")
public class EntityArtikel {

    @Id
    @Column(name="id")
    int id;
    @Column(name="artNr")
    int artNr;
    @Column(name="artBezeichnung")
    String artBezeichnung;
    @Column(name="preis")
    Double preis;

    public EntityArtikel(int id, int artNr, String artBezeichnung, Double preis) {
        this.id = id;
        this.artNr = artNr;
        this.artBezeichnung = artBezeichnung;
        this.preis = preis;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getArtNr() {
        return artNr;
    }

    public void setArtNr(int artNr) {
        this.artNr = artNr;
    }

    public String getArtBezeichnung() {
        return artBezeichnung;
    }

    public void setArtBezeichnung(String artBezeichnung) {
        this.artBezeichnung = artBezeichnung;
    }

    public Double getPreis() {
        return preis;
    }

    public void setPreis(Double preis) {
        this.preis = preis;
    }
}

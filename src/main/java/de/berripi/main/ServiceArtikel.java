package de.berripi.main;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.ArrayList;
import java.util.List;

@Service
public class ServiceArtikel {

    private EntityManagerFactory emf;

    @Autowired
    public ServiceArtikel(EntityManagerFactory emf) {
        Assert.notNull(emf, "EntityManagerFactory darf nicht null sein");
        this.emf = emf;
    }

    public List<EntityArtikel> insertArtikel(EntityArtikel[] entityArtikel){
        EntityManager entityManager = emf.createEntityManager();
        List<EntityArtikel> artikelListe = new ArrayList<>();
        int batchSize = 4;
        int n = 0;

        entityManager.getTransaction().begin();
        for(EntityArtikel item : entityArtikel){
            EntityArtikel artikel = new EntityArtikel(
                    item.getId(),
                        item.getArtNr(),
                            item.getArtBezeichnung(),
                                item.getPreis());
            artikelListe.add(artikel);

            if(++n % batchSize == 0){
                entityManager.flush();
                entityManager.clear();
                artikelListe.clear();
            }

            artikelListe.forEach(art -> entityManager.persist(art));
        }
        entityManager.getTransaction().commit();
        entityManager.close();
        return artikelListe;
    }
}

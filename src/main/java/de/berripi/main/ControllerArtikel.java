package de.berripi.main;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

@Controller
public class ControllerArtikel {

    @Autowired
    ServiceArtikel serviceArtikel;

    @PostMapping(value="/publ/insertArtikel", consumes = "application/json", produces = "application/json")
    public ResponseEntity insertArtikel(@RequestBody EntityArtikel[] entityArtikel){
        List<EntityArtikel> artikelListe = serviceArtikel.insertArtikel(entityArtikel);
        return ResponseEntity.ok(artikelListe);
    }
}

package de.berripi.main;

import org.springframework.data.jpa.repository.support.SimpleJpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

@Repository
public class RepositorySimpleJpa extends SimpleJpaRepository<EntityArtikel, Long> {

    private EntityManager entityManager;

    public RepositorySimpleJpa(EntityManager entityManager) {
        super(EntityArtikel.class, entityManager);
        this.entityManager=entityManager;
    }

    @Transactional
    public List<EntityArtikel> save(List<EntityArtikel> items) {
        items.forEach(item -> entityManager.persist(item));
        return items;
    }
}

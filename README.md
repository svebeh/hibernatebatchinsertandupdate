# Hibernate Batch Insert und Update #

Java / Spring Boot / H2-DB Template.  
Diese Application dienst als Basis Template für Massen-Inserts und Massen-Updates in Datenbanken. Hibernate Batch wird in der application.properties konfiguriert.
Zum Testen ist die spring.jpa.properties.hibernate.jdbc.batch_size auf 4 gesetzt, später sollte dieser Wert optimalerweise zwischen 20 und 50 liegen.  

Die Batch Konfiguration in der application.properties:  
```
#Batch Update
spring.jpa.properties.hibernate.jdbc.batch_size=4
spring.jpa.properties.hibernate.order_updates=true
spring.jpa.properties.hibernate.order_inserts=true
spring.jpa.properties.hibernate.batch_versioned_data=true
spring.jpa.properties.hibernate.generate_statistics=true
```


### Der Insert Webservice Aufruf ###
Http.Method=POST
```
http://localhost:8080/publ/insertArtikel
```
Im RequestBody muss ein JSON Array übergeben werden. Zum Testen kann das folgenden JSON genutzt werden:  
```
[
    {
        "id":1,
        "artNr":1001,
        "artBezeichnung":"Schachspiel",
        "preis":19.99
    },
    {
        "id":2,
        "artNr":1002,
        "artBezeichnung":"Muehle",
        "preis":15.99
    },
    {
        "id":3,
        "artNr":1003,
        "artBezeichnung":"Handy",
        "preis":198.50
    },
    {
        "id":4,
        "artNr":1004,
        "artBezeichnung":"Uhr",
        "preis":34.99
    },
    {
        "id":5,
        "artNr":1005,
        "artBezeichnung":"Blume",
        "preis":5.50
    },
    {
        "id":6,
        "artNr":1006,
        "artBezeichnung":"TV",
        "preis":599.99
    },
    {
        "id":7,
        "artNr":1007,
        "artBezeichnung":"Feuerzeug",
        "preis":1.99
    },
    {
        "id":8,
        "artNr":1008,
        "artBezeichnung":"Kartenspiel",
        "preis":2.99
    }
]
```


### H2 Datenbank ###

* H2 Console: http://localhost:8080/h2-console
* driverClass=org.h2.Driver
* url=jdbc:h2:mem:testdb
* username=sa
* password=
* !Das Passwort bleibt leer!  


### Application starten ###
(Im Terminal) Projekt klonen --> in das Verzeichnis wechseln --> den folgenden Befehl im Terminal ausführen:
```
mvn clean install spring-boot:run
```
